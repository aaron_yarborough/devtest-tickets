﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using TruePotentialDevTest.Models;

namespace TruePotentialDevTest.Data
{
    public class TPConfiguration : DbMigrationsConfiguration<TPContext>
    {
        public TPConfiguration()
        {
            AutomaticMigrationDataLossAllowed = true;
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(TPContext context)
        {
            var users = new List<User>
            {
                new User { Email = "personal.ajy@gmail.com", FullName = "Aaron Yarborough", Password = "567ea4868b532a5d837e69d56bf10e79", UserType = User.Type.Admin },
                new User { Email = "admin@truepotential.com", FullName = "TP (Admin)", Password = "5f4dcc3b5aa765d61d8327deb882cf99", UserType = User.Type.Admin },
                new User { Email = "client@truepotential.com", FullName = "TP (Client)", Password = "5f4dcc3b5aa765d61d8327deb882cf99", UserType = User.Type.Client }
            };

            users.ForEach(e => context.Users.Add(e));
            context.SaveChanges();

            base.Seed(context);
        }
    }
}