﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TruePotentialDevTest.Models;

namespace TruePotentialDevTest.Controllers
{
    public class AlertController : Controller
    {
        // GET: Alert
        public ActionResult Index(AlertViewModel alert)
        {
            return View(alert);
        }
    }
}