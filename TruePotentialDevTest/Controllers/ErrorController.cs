﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TruePotentialDevTest.Models;

namespace TruePotentialDevTest.Controllers
{
    public class ErrorController : Controller
    {
        public ActionResult Index(ErrorViewModel error)
        {
            if (String.IsNullOrEmpty(error.Message))
            {
                error = new ErrorViewModel();
                error.Message = "An error message has not been provided!";
            }
               
            return View(error);
        }
    }
}