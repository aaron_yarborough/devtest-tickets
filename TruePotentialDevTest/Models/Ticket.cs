﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlTypes;
using System.Linq;
using System.Web;

namespace TruePotentialDevTest.Models
{
    public class Ticket
    {
        public enum TicketStatus
        {
            Unclaimed,
            Claimed,
            Resolved,
            Closed
        }

        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public DateTime CreationDate { get; set; }

        public DateTime? UpdateDate { get; set; }

        public virtual User Creator { get; set; }

        public virtual User Holder { get; set; }

        public virtual List<Response> Responses { get; set; }

        public int Rating { get; set; }

        public string Content { get; set; }

        public TicketStatus Status { get; set; }
    }
}