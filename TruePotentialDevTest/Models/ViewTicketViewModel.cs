﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TruePotentialDevTest.Models
{
    public class ViewTicketViewModel
    {
        public Ticket Ticket { get; set; }

        public string ResponseContent { get; set; }
    }
}