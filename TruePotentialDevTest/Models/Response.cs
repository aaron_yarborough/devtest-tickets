﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TruePotentialDevTest.Models
{
    public class Response
    {
        [Key]
        public int Id { get; set; }

        public virtual User Creator { get; set; }

        public DateTime CreationDate { get; set; }

        public string Content { get; set; }

        public int TicketId { get; set; }
    }
}