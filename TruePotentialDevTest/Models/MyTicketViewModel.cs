﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TruePotentialDevTest.Models
{
    public class MyTicketViewModel
    {
        public List<ListTicketViewModel> Tickets { get; set; }

        public MyTicketViewModel()
        {
            // Create a new instance of the list so *.Add() can be called
            // on it to add new tickets
            Tickets = new List<ListTicketViewModel>();
        }
    }
}