﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TruePotentialDevTest.Data;
using TruePotentialDevTest.Models;

namespace TruePotentialDevTest.Business
{
    public class ResponseBusiness : BusinessBase
    {
        /// <summary>
        /// Adds a new response to the database
        /// </summary>
        /// <param name="ticketId">Ticked ID</param>
        /// <param name="creatorId">Creator ID</param>
        /// <param name="content">Response text</param>
        public void AddResponse(int ticketId, int creatorId, string content)
        {
            Response response = new Response();
            response.CreationDate = DateTime.Now;
            response.Content = content;
            response.TicketId = ticketId;

            using (UserBusiness userBusiness = new UserBusiness())
                response.Creator = userBusiness.GetUser(creatorId);

            using (TPContext context = new TPContext())
            {
                Ticket ticket = context.Tickets.FirstOrDefault(x => x.Id == ticketId);
                ticket.Holder = response.Creator;
                ticket.Status = Ticket.TicketStatus.Claimed;

                context.Tickets.Attach(ticket);
                context.Entry(ticket).State = EntityState.Modified;

                context.Set<Response>().Add(response);

                context.SaveChanges();
            }
        }
    }
}