﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TruePotentialDevTest.Data;
using TruePotentialDevTest.Models;

namespace TruePotentialDevTest.Business
{
    public class UserBusiness : BusinessBase
    {
        /// <summary>
        /// Check the database to see if the specified email is already
        /// tied to a user account, therefore checking if a user already
        /// exists under that email
        /// </summary>
        /// <param name="email">Email address</param>
        /// <returns>Result</returns>
        public bool IsUser(string email)
        {
            bool exists;
            using (TPContext context = new TPContext())
            {
                exists = context.Users.Any(x => x.Email == email);
            }

            return exists;
        }

        /// <summary>
        /// Gets a user with a specific email address
        /// </summary>
        /// <param name="email">Email address</param>
        /// <returns>User</returns>
        public User GetUser(string email)
        {
            User toReturn;
            using (TPContext context = new TPContext())
            {
                toReturn = context.Users.FirstOrDefault(x => x.Email == email);
            }

            return toReturn;
        }

        /// <summary>
        /// Gets a user with a specific id
        /// </summary>
        /// <param name="id">id</param>
        /// <returns>User</returns>
        public User GetUser(int id)
        {
            User toReturn;
            using (TPContext context = new TPContext())
            {
                toReturn = context.Users.FirstOrDefault(x => x.Id == id);
            }

            return toReturn;
        }
    }
}